package com.faeda.app.utils;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.faeda.app.R;
import com.faeda.app.activity.Detail;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by jain on 30-Mar-17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Model> dataSet;
    Context mContext;
    int total_types;
    MediaPlayer mPlayer;
    private boolean fabStateVolume = false;

    public static class TextTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtType;
        TextView description;
        TextView textPrice;
        TextView textLocation;
        CardView cardView;
        Button button;

        public TextTypeViewHolder(View itemView) {
            super(itemView);
            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.cardView = (CardView) itemView.findViewById(R.id.card_view);
            this.description = (TextView) itemView.findViewById(R.id.description);
            this.button = (Button) itemView.findViewById(R.id.buttonInterest);
            this.textPrice = (TextView) itemView.findViewById(R.id.textPrice);
            this.textLocation = (TextView) itemView.findViewById(R.id.textLocation);
        }
    }

    public static class ImageTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtType;
        ImageView image;

        public ImageTypeViewHolder(View itemView) {
            super(itemView);
            this.txtType = (TextView) itemView.findViewById(R.id.type);
            this.image = (ImageView) itemView.findViewById(R.id.background);
        }
    }


    public RecyclerAdapter(ArrayList<Model> data, Context context) {
        this.dataSet = data;
        this.mContext = context;
        total_types = dataSet.size();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case Model.TEXT_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_image, parent, false);
                return new TextTypeViewHolder(view);
            case Model.IMAGE_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_text, parent, false);
                return new ImageTypeViewHolder(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).type) {
            case 0:
                return Model.TEXT_TYPE;
            case 1:
                return Model.IMAGE_TYPE;
            default:
                return -1;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        final Model object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.type) {
                case Model.TEXT_TYPE:
                    final ArrayList<String> row = object.data;
                    Log.e("FROM ROW", row.toString());
                    final String id = row.get(0);
                    String title = row.get(1);
                    String description = row.get(2);
                    String location = row.get(4);
                    String price = "Rp "+row.get(8)+".00";
                    ((TextTypeViewHolder) holder).txtType.setText(title);
                    ((TextTypeViewHolder) holder).description.setText(description);
                    ((TextTypeViewHolder) holder).textPrice.setText(price);
                    ((TextTypeViewHolder) holder).textLocation.setText(location);
                    ((TextTypeViewHolder) holder).button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.e("INI ID", id);
                            // on click event open another activity or something
                            Intent nextScreen = new Intent(mContext, Detail.class);
                            nextScreen.putStringArrayListExtra("dataarraylist",row);
                            mContext.startActivity(nextScreen);

                        }
                    });


                    break;
                case Model.IMAGE_TYPE:
                    ((ImageTypeViewHolder) holder).txtType.setText(" This is Image with Text type Cardview. Fetch Image using Volley and Glide.");
                    // ((ImageTypeViewHolder) holder).image.setImageResource(object.data);
                    Glide.with(mContext).load(object.data)
                            .thumbnail(0.5f)
                            .crossFade()
                            //.dontAnimate()
                            .placeholder(R.drawable.motivation)
                            // .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(((ImageTypeViewHolder) holder).image);

                    ((ImageTypeViewHolder) holder).image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // intent to start new activity for youtube player....
                            // on click event open another activity or something

                            //    Intent nextScreen = new Intent(mContext, OpenFullDetail.class);
                            //    mContext.startActivity(  nextScreen);
                        }
                    });


                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


}
