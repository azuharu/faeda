package com.faeda.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.faeda.app.R;
import com.faeda.app.utils.AppController;
import com.faeda.app.utils.Model;
import com.faeda.app.utils.RecyclerAdapter;
import com.faeda.app.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Dashboard extends AppCompatActivity {
    private String TAG ="dashboard";
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private ArrayList<Model> list;
    private RecyclerAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private FloatingActionButton post_thread;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, PageCount=1, PageCountLock=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initToolbar();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        post_thread = (FloatingActionButton) findViewById(R.id.post_thread);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(Dashboard.this, OrientationHelper.VERTICAL, false);
        list= new ArrayList<>();

        post_thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(getApplicationContext(), PostThread.class);
                startActivity(x);
            }
        });
        //list.add(new Model(Model.IMAGE_TYPE,"http://blueappsoftware.in/android/downloadcode/DemoImage/learnwithkamalbunkar.png"));
        String apiUrl = "http://instasos.net/thread.php";

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, apiUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray threads = response.getJSONArray("threads");
                    for(int i = 0; i< threads.length();i++)
                    {
                        JSONObject thread = threads.getJSONObject(i);
                        ArrayList<String> row = new ArrayList<String>();
                        String id = thread.getString("id");
                        String title = thread.getString("title");
                        String description = thread.getString("description");
                        if(description.length()>400) {
                            description = description.substring(0, 400) + "...";
                        }
                        String image_thread = thread.getString("image_thread");
                        String location = thread.getString("location");
                        String date_start = thread.getString("date_start");
                        String date_end = thread.getString("date_end");
                        String reward_type = thread.getString("reward_type");
                        String reward_kind = thread.getString("price");
                        String category = thread.getString("category");
                        String number_people = thread.getString("orang_yang_dibutuhkan");
                        row.add(id);
                        row.add(title);
                        row.add(description);
                        row.add(image_thread);
                        row.add(location);
                        row.add(date_start);
                        row.add(date_end);
                        row.add(reward_type);
                        row.add(reward_kind);
                        row.add(category);
                        row.add(number_people);
                        Log.e("FROM DASHBOARD", row.toString());
                        list.add(new Model(Model.TEXT_TYPE, row));
                    }
                } catch (JSONException e) {
                    Log.e("ERROR", e.toString());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error.Response", error.toString());
            }
        });

        requestQueue.add(getRequest);

        adapter = new RecyclerAdapter(list,this);


        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

    }

    public void onClick(View v) {
        int id = v.getId();

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Tools.setSystemBarColor(this, R.color.colorPrimary);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, MainActivity.class);
                this.startActivity(intent);
                break;
            case R.id.action_profile:
                Intent intent2 = new Intent(this, Profile.class);
                this.startActivity(intent2);
                break;
            case R.id.action_login:
                Intent intent3 = new Intent(this, Login.class);
                this.startActivity(intent3);
                break;
            case R.id.action_register:
                Intent intent4 = new Intent(this, Register.class);
                this.startActivity(intent4);
                break;
            case R.id.action_filter:
                Intent intent6 = new Intent(this, Filter.class);
                this.startActivity(intent6);
                break;
             default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

}
